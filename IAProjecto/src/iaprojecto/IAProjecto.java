/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iaprojecto;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFrame;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import java.awt.Color;

/**
 *
 * @author eygonzalez
 */
public class IAProjecto extends JFrame implements KeyListener{

    JPanel p = new JPanel();
    JTextArea dialog = new JTextArea(20,50);
    JTextArea input = new JTextArea(1,50);
    JScrollPane scroll = new JScrollPane(
        dialog,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    
    String H = "es un humano";
    String M = "es mortal";
    String D = "es un Dios";
    String P = "tiene poderes sobrenaturales";
    String r = "Richard ";
    String n = "NO ";
    String ant;

    public static void main(String[] args) {
        new IAProjecto();
    }
    
    public IAProjecto(){
        super ("INTELIGENCIA ARTIFICIAL");
        setSize(600,400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        dialog.setEditable(false);
        input.addKeyListener(this);
        
        p.add(scroll);
        p.add(input);
        p.setBackground(Color.GREEN);
        add(p);
        
        setVisible(true);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            input.setEditable(false);
            
            //------Grabar Texto-------
            String texto = input.getText();
            input.setText("");
            addText("--> Tu:\t" + texto);
            texto.trim();
            
            while (
                    texto.charAt(texto.length() -1) == '!' ||
                    texto.charAt(texto.length() -1) == '?' ||
                    texto.charAt(texto.length() -1) == '.' ||
                    texto.charAt(texto.length() -1) == ','
                    ){
                texto = texto.substring(0, texto.length() -1);
            }
            texto.trim();

            if (texto.length() == r.length()+H.length()){
                addText("\n--> Armani:\t" +r+M);
                addText("\n--> Armani:\t" +r+n+D);
            }
            if (texto.length() == r.length()+D.length()){
                addText("\n--> Armani:\t" +r+D +" Y " +r+n+D);
                addText("\n--> Armani:\t" +r+n+D);
            }
            if (texto.length() == r.length()+P.length()){
                addText("\n--> Armani:\t" +r+D);
                addText("\n--> Armani:\t" +r+P+" Y "+r+D);
            }
            
            addText("\n");
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            input.setEditable(true);
        }
    }
    
    public void keyTyped(KeyEvent e) {
        
    }
    
    public void addText(String Str){
        dialog.setText(dialog.getText() + Str);
    }
    
}

